import 'package:ayobersuara/utils/color_utils.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter/material.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  State<SignUpPage> createState() => __SignUpPageState();
}

class __SignUpPageState extends State<SignUpPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text('Image'),
        leading: Image.asset(
          'assets/images/new logo.png',
          height: 40,
          width: 40,
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          color: Color.fromRGBO(255, 255, 255, 1),
        ),
        child: Stack(
          children: <Widget>[
            Positioned(
                top: 53,
                left: 106,
                child: Container(
                  width: 178,
                  height: 34,
                ))
          ],
        ),
      ),
    );
  }
}
