import 'package:flutter/material.dart';
// import 'dart:math' as math;

class LoginpremiumroomadminpageonlyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Figma Flutter Generator LoginpremiumroomadminpageonlyWidget - FRAME
    return Container(
        width: 390,
        height: 844,
        decoration: BoxDecoration(
          color: Color.fromRGBO(255, 255, 255, 1),
        ),
        child: Stack(children: <Widget>[
          Positioned(
              top: 413,
              left: 23,
              child: Text(
                'Login\nPremium Room Administrator',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Color.fromRGBO(0, 0, 0, 1),
                    fontFamily: 'Roboto Slab',
                    fontSize: 24,
                    letterSpacing:
                        0 /*percentages not used in flutter. defaulting to zero*/,
                    fontWeight: FontWeight.normal,
                    height: 1),
              )),
          Positioned(
              top: 763,
              left: 33,
              child: Container(
                  width: 323,
                  height: 43,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                    ),
                    color: Color.fromRGBO(80, 124, 182, 1),
                  ),
                  child: Stack(children: <Widget>[
                    Positioned(
                        top: 11,
                        left: 139,
                        child: Text(
                          'Login',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              fontFamily: 'Roboto Slab',
                              fontSize: 16,
                              letterSpacing:
                                  0 /*percentages not used in flutter. defaulting to zero*/,
                              fontWeight: FontWeight.normal,
                              height: 1),
                        )),
                  ]))),
          Positioned(
              top: 579,
              left: 29,
              child: Container(
                  width: 332,
                  height: 40,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                    ),
                    boxShadow: [
                      BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.25),
                          offset: Offset(0, 0),
                          blurRadius: 5)
                    ],
                    color: Color.fromRGBO(255, 255, 255, 1),
                  ),
                  child: Stack(children: <Widget>[
                    Positioned(
                        top: 12,
                        left: 22,
                        child: Text(
                          'Email',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(161, 161, 161, 1),
                              fontFamily: 'Roboto Slab',
                              fontSize: 12,
                              letterSpacing:
                                  0 /*percentages not used in flutter. defaulting to zero*/,
                              fontWeight: FontWeight.normal,
                              height: 1),
                        )),
                  ]))),
          Positioned(
              top: 643,
              left: 29,
              child: Container(
                  width: 332,
                  height: 40,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10),
                    ),
                    boxShadow: [
                      BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.25),
                          offset: Offset(0, 0),
                          blurRadius: 5)
                    ],
                    color: Color.fromRGBO(255, 255, 255, 1),
                  ),
                  child: Stack(children: <Widget>[
                    Positioned(
                        top: 12,
                        left: 22,
                        child: Text(
                          'Password',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(161, 161, 161, 1),
                              fontFamily: 'Roboto Slab',
                              fontSize: 12,
                              letterSpacing:
                                  0 /*percentages not used in flutter. defaulting to zero*/,
                              fontWeight: FontWeight.normal,
                              height: 1),
                        )),
                  ]))),
          Positioned(
              top: 482,
              left: 34,
              child: Text(
                'Bersama kami, melakukan vote menjadi lebih\nmudah dan aman ',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Color.fromRGBO(0, 0, 0, 0.6000000238418579),
                    fontFamily: 'Roboto Slab',
                    fontSize: 14,
                    letterSpacing:
                        0 /*percentages not used in flutter. defaulting to zero*/,
                    fontWeight: FontWeight.normal,
                    height: 1),
              )),
          Positioned(
              top: 174,
              left: 29,
              child: Container(
                  width: 331,
                  height: 220,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/343862802.png'),
                        fit: BoxFit.fitWidth),
                  ))),
          Positioned(
              top: 53,
              left: 106,
              child: Container(
                  width: 178,
                  height: 34,
                  child: Stack(children: <Widget>[
                    Positioned(
                        top: 0,
                        left: 36,
                        child: Text(
                          'yoBersuara',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(80, 124, 182, 1),
                              fontFamily: 'Roboto Slab',
                              fontSize: 26,
                              letterSpacing:
                                  0 /*percentages not used in flutter. defaulting to zero*/,
                              fontWeight: FontWeight.normal,
                              height: 1),
                        )),
                    Positioned(
                        top: 0,
                        left: 0,
                        child: Container(
                            width: 34,
                            height: 34,
                            decoration: BoxDecoration(
                              color: Color.fromRGBO(80, 124, 182, 1),
                              borderRadius:
                                  BorderRadius.all(Radius.elliptical(34, 34)),
                            ))),
                    Positioned(
                        top: 0,
                        left: 7,
                        child: Text(
                          'A',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              fontFamily: 'Roboto Slab',
                              fontSize: 26,
                              letterSpacing:
                                  0 /*percentages not used in flutter. defaulting to zero*/,
                              fontWeight: FontWeight.normal,
                              height: 1),
                        )),
                  ]))),
          // Positioned(
          //   top: 70,
          //   left: 47,
          //   child: Transform.rotate(
          //   angle: -180 * (math.pi / 180),
          //   child: SvgPicture.asset(
          //   'assets/images/arrow1.svg',
          //   semanticsLabel: 'arrow1'
          // );,
          // )
          // ),
        ]));
  }
}
